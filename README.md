#NOTE ON CLONE
To clone the vim bundles (which are git projects themselves), you need to clone with:
`git clone --recurse-submodules https://gitlab.com/dclayto1/dotfiles.git`

If you forgot to use the `--recurse-submodules` flag, the vim bundles will be empty. Run these 2 commands from the root of the project:
1. `git submodule init`
2. `git submodule update`

#Possible Pre-requisites
* zsh
* xcompmgr
* feh
* i3wm
* i3lock
* i3status
* i3bar
* dunst
* tmux
* vim
* cava
* powerline-fonts (install script provided)
 * Run `install_powerline-fonts.zsh`
* pyenv (install script provided)
 * Run `install_pyenv.zsh`
* nvm (install script provided)
 * Run `install_nvm.zsh`

#Installation
**Installing these files WILL remove files of the same name. Please create backup copies of yours first**
1. `cd` into the root directory of this project
2. Run `install.zsh <install_type>`
3. You may need to logout and login for effects to take place

##Install Types (See 'File Locations On Install' for requirements)
* Individual Installs
 * dunst
  * installs 'dunst' files
 * cava
  * installs 'cava' files
 * i3 tivodesktop
  * installs 'i3' files from i3/tivodesktop
 * i3 archlaptop
  * installs 'i3' files from i3/archlaptop
 * tools
  * installs 'tools' files
 * zsh
  * installs 'zsh' files
 * tmux
  * installs 'tmux' files
 * vim
  * installs 'vim' files
 * wallpapers
  * installs 'wallpapers' files
* Multi Installs
 * archlaptop
  * dunst
  * i3 archlaptop
  * tools
  * zsh
  * tmux
  * vim
  * cava
 * tivodesktop
  * dunst
  * i3 tivodesktop
  * tools
  * zsh
  * tmux
  * vim
  * cava
 * terminal
  * tools
  * zsh
  * tmux
  * vim
  * cava
 

##File Locations On Install
* dunst (requires: dunst)
 * ~/.config/dunst/dunstrc
* cava (requires: cava)
 * ~/.config/cava/config
* i3 (requires: i3wm, i3lock, i3status, i3bar, xcompmgr, feh)
 * ~/.config/i3/config
 * ~/.config/i3status/config 
 * ~/.xprofile (must configure the wallpaper)
* tools (requires: tmux)
 * ~/bin/derby-cli (requires manual download of derby tools)
 * ~/bin/dev-tmux
 * ~/bin/getTivoStock.py
 * ~/bin/si-tmux
 * ~/bin/convert2gif
 * ~/bin/record
 * ~/bin/externalServicesFFS
 * ~/bin/ffs
 * ~/bin/vscode
* zsh (requires: zsh, powerline-fonts, pyenv, nvm)
 * ~/.zshrc
 * ~/.\_functions
 * ~/.\_aliases
 * ~/lib/git.zsh
 * ~/theme.powerline.theme
* tmux (requires: tmux)
 * ~/.tmux.conf
* vim (requires: vim)
 * ~/.vimrc
 * ~/.vim/autoload/pathogen.vim
 * ~/.vim/bundle/denite.nvim
 * ~/.vim/bundle/typescript-vim
 * ~/.vim/bundle/unite.vim
 * ~/.vim/bundle/vimfiler.vim
 * ~/.vim/bundle/vim-gitgutter
* wallpapers
 * ~/.wallpapers/red-black-abstract.png
 * ~/.wallpapers/green-black-abstract.png
