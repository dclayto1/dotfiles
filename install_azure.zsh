#!/bin/zsh
DOT="$HOME/dotfiles"
cd $DOT

git submodule init
git submodule update

$DOT/install.zsh azure
$DOT/install_nvm.zsh
$DOT/install_pyenv.zsh

echo "export USER=vsonline" >> $HOME/.zshrc
echo "export OMIT_HOSTNAME=1" >> $HOME/.zshrc
