# clone into /tmp
cd /tmp
git clone https://github.com/powerline/fonts.git --depth=1

# install
cd fonts
./install.sh

# clean-up a bit
cd ..
rm -rf fonts

# return to dotfile install directory
cd $CUR_DIR
