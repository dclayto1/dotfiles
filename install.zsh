#!/bin/zsh
CUR_DIR=$PWD

#####################
#INDIVIDUAL INSTALLS#
#####################
type_dunst () {
  echo "Installing dunst files"
  mkdir -p $HOME/.config/dunst
  rm $HOME/.config/dunst/dunstrc

  ln -s $CUR_DIR/dunst/dunstrc $HOME/.config/dunst/dunstrc
}

type_cava () {
  echo "Installing cava files"
  mkdir -p $HOME/.config/cava
  rm $HOME/.config/cava/config
  ln -s $CUR_DIR/cava/config $HOME/.config/cava/config
}

type_i3_tivodesktop () {
  echo "Installing i3 files for tivodesktop"
  mkdir -p $HOME/.config/i3
  mkdir -p $HOME/.config/i3status
  rm $HOME/.config/i3/config
  rm $HOME/.config/i3status/config
  rm $HOME/.xprofile

  ln -s $CUR_DIR/i3/tivodesktop/i3config $HOME/.config/i3/config
  ln -s $CUR_DIR/i3/tivodesktop/i3statusconfig $HOME/.config/i3status/config
  ln -s $CUR_DIR/i3/tivodesktop/xprofile $HOME/.xprofile
}

type_i3_archlaptop () {
  echo "Installing i3 files for archlaptop"
  mkdir -p $HOME/.config/i3
  mkdir -p $HOME/.config/i3status
  rm $HOME/.config/i3/config
  rm $HOME/.config/i3status/config
  rm $HOME/.xprofile

  ln -s $CUR_DIR/i3/archlaptop/i3config $HOME/.config/i3/config
  ln -s $CUR_DIR/i3/archlaptop/i3statusconfig $HOME/.config/i3status/config
  ln -s $CUR_DIR/i3/archlaptop/xprofile $HOME/.xprofile
}

type_tools () {
  echo "Installing tools files"
  mkdir -p $HOME/bin
  rm $HOME/bin/derby-cli
  rm $HOME/bin/dev-tmux
  rm $HOME/bin/getTivoStock.py
  rm $HOME/bin/si-tmux
  rm $HOME/bin/record
  rm $HOME/bin/convert2gif
  rm $HOME/bin/externalServicesFFS
  rm $HOME/bin/ffs
  rm $HOME/bin/vscode

  ln -s $CUR_DIR/tools/derby-cli $HOME/bin/derby-cli
  ln -s $CUR_DIR/tools/dev-tmux $HOME/bin/dev-tmux
  ln -s $CUR_DIR/tools/getTivoStock.py $HOME/bin/getTivoStock.py
  ln -s $CUR_DIR/tools/si-tmux $HOME/bin/si-tmux
  ln -s $CUR_DIR/tools/record $HOME/bin/record
  ln -s $CUR_DIR/tools/convert2gif $HOME/bin/convert2gif
  ln -s $CUR_DIR/tools/externalServicesFFS $HOME/bin/externalServicesFFS
  ln -s $CUR_DIR/tools/ffs $HOME/bin/ffs
  ln -s $CUR_DIR/tools/vscode $HOME/bin/vscode
}

type_zsh () {
  echo "Installing zsh files"
  mkdir -p $HOME/.config/zsh/lib
  mkdir -p $HOME/.config/zsh/theme
  rm $HOME/.zshrc
  rm $HOME/.config/zsh/lib/git.zsh
  rm $HOME/.config/zsh/theme/powerline.theme
  rm $HOME/.config/zsh/theme/monospace.theme
  rm $HOME/._functions
  rm $HOME/._aliases

  ln -s $CUR_DIR/zsh/zshrc $HOME/.zshrc
  ln -s $CUR_DIR/zsh/_functions $HOME/._functions
  ln -s $CUR_DIR/zsh/_aliases $HOME/._aliases
  ln -s $CUR_DIR/zsh/lib/git.zsh $HOME/.config/zsh/lib/git.zsh
  ln -s $CUR_DIR/zsh/theme/powerline.theme $HOME/.config/zsh/theme/powerline.theme
  ln -s $CUR_DIR/zsh/theme/monospace.theme $HOME/.config/zsh/theme/monospace.theme
}

type_tmux () {
  echo "Installing tmux files"
  rm $HOME/.tmux.conf

  ln -s $CUR_DIR/tmux/tmux.conf $HOME/.tmux.conf
}

type_vim () {
  echo "Installing vim files"
  mkdir -p $HOME/.vim/autoload
  mkdir -p $HOME/.vim/bundle
  rm $HOME/.vimrc
  rm $HOME/.vim/autoload/pathogen.vim
  rm -rf $HOME/.vim/bundle/denite.nvim
  rm -rf $HOME/.vim/bundle/typescript-vim
  rm -rf $HOME/.vim/bundle/unite.vim
  rm -rf $HOME/.vim/bundle/vimfiler.vim
  rm -rf $HOME/.vim/bundle/vim-gitgutter
  rm -rf $HOME/.vim/bundle/vim-vue

  ln -s $CUR_DIR/vim/vimrc $HOME/.vimrc
  ln -s $CUR_DIR/vim/pathogen/pathogen.vim $HOME/.vim/autoload/pathogen.vim
  ln -s $CUR_DIR/vim/pathogen/bundles/denite.nvim $HOME/.vim/bundle/denite.nvim
  ln -s $CUR_DIR/vim/pathogen/bundles/typescript-vim $HOME/.vim/bundle/typescript-vim
  ln -s $CUR_DIR/vim/pathogen/bundles/unite.vim $HOME/.vim/bundle/unite.vim
  ln -s $CUR_DIR/vim/pathogen/bundles/vimfiler.vim $HOME/.vim/bundle/vimfiler.vim
  ln -s $CUR_DIR/vim/pathogen/bundles/vim-gitgutter $HOME/.vim/bundle/vim-gitgutter
  ln -s $CUR_DIR/vim/pathogen/bundles/vim-vue $HOME/.vim/bundle/vim-vue
}

type_wallpapers () {
  echo "Installing wallpaper files"
  mkdir -p $HOME/.wallpapers
  ln -s $CUR_DIR/wallpapers/red-black-abstract.png $HOME/.wallpapers/red-black-abstract.png
  ln -s $CUR_DIR/wallpapers/green-black-abstract.png $HOME/.wallpapers/green-black-abstract.png
}

################
#MULTI INSTALLS#
################
type_terminal () {
  type_tools
  type_zsh
  type_tmux
  type_vim
  type_cava
}

type_minus_i3 () {
  type_terminal
  type_dunst
}

type_archlaptop () {
  type_minus_i3
  type_i3_archlaptop
}

type_tivodesktop () {
  type_minus_i3
  type_i3_tivodesktop
}

type_azure () {
  type_tools
  type_zsh
  type_vim
}


if [[ "$1" == "dunst" ]]; then
  type_dunst
elif [[ "$1" == "cava" ]]; then
  type_cava
elif [[ "$1" == "tools" ]]; then
  type_tools
elif [[ "$1" == "zsh" ]]; then
  type_zsh
elif [[ "$1" == "tmux" ]]; then
  type_tmux
elif [[ "$1" == "vim" ]]; then
  type_vim
elif [[ "$1" == "wallpapers" ]]; then
  type_wallpapers
elif [[ "$1" == "i3" ]]; then
  if [[ "$2" == "tivodesktop" ]]; then
    type_i3_tivodesktop
  elif [[ "$2" == "archlaptop" ]]; then
    type_i3_archlaptop
  else
    echo "Refer to the README for proper i3 install arguments"
  fi
elif [[ "$1" == "archlaptop" ]]; then
  type_archlaptop
elif [[ "$1" == "tivodesktop" ]]; then
  type_tivodesktop
elif [[ "$1" == "terminal" ]]; then
  type_terminal
elif [[ "$1" == "azure" ]]; then
  type_azure
else
  echo "Refer to the README for proper install arguments"
fi
